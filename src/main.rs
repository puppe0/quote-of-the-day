extern crate quote_of_the_day;

use actix_web::{App, HttpServer};
use quote_of_the_day::api::{quote_by_category, random_quote};
use std::env;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let host = "0.0.0.0";

    let port = env::var("PORT")
        .unwrap_or_else(|_| "8000".to_string())
        .parse()
        .expect("PORT must be a number");

    println!("Server started at {}:{}", host, port);

    HttpServer::new(|| App::new().service(random_quote).service(quote_by_category))
        .bind((host, port))?
        .run()
        .await
}
