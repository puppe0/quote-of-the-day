pub mod api {
    use actix_web::{get, web, HttpResponse, Responder};
    use rand::seq::SliceRandom;
    use serde::{Deserialize, Serialize};
    use std::collections::HashMap;

    #[derive(Serialize, Deserialize)]
    pub struct QuoteResponse {
        pub quote: String,
        pub category: String,
    }

    #[derive(Serialize)]
    struct NotFoundResponse {
        error: String,
    }

    #[get("/quotes/{category}")]
    async fn quote_by_category(category: web::Path<String>) -> impl Responder {
        let cat = category.into_inner();

        if let Some(quote) = get_quote(&cat) {
            HttpResponse::Ok().json(QuoteResponse {
                quote: quote.to_string(),
                category: cat,
            })
        } else {
            HttpResponse::NotFound().json(NotFoundResponse {
                error: String::from("Category not found"),
            })
        }
    }

    #[get("/quotes/random")]
    async fn random_quote() -> impl Responder {
        let quotes = get_quotes();

        let mut categories = Vec::new();
        for (key, _) in quotes.iter() {
            categories.push(key);
        }

        if let Some(category) = categories.choose(&mut rand::thread_rng()) {
            if let Some(quote) = get_quote(category) {
                HttpResponse::Ok().json(QuoteResponse {
                    quote: quote.to_string(),
                    category: category.to_string(),
                })
            } else {
                HttpResponse::NotFound().json(NotFoundResponse {
                    error: String::from("Category not found"),
                })
            }
        } else {
            HttpResponse::NotFound().json(NotFoundResponse {
                error: String::from("Category not found"),
            })
        }
    }

    fn get_quote(category: &String) -> Option<String> {
        get_quotes()
            .get(category)
            .and_then(|category_quotes| category_quotes.choose(&mut rand::thread_rng()))
            .map(|quote| quote.to_string())
    }

    fn get_quotes() -> HashMap<String, Vec<String>> {
        let mut quotes = HashMap::new();

        quotes.insert(
            String::from("fundamentals"),
            vec![
                String::from("Keep it simple, stupid. You ain't gonna need it."),
                String::from("You should think about what to do before you do it."),
                String::from("You should try to talk about what you’re planning to do before you do it."),
                String::from("You should think about what you did after you did it."),
                String::from("Be prepared to throw away something you’ve done in order to do something different."),
                String::from("Always look for better ways of doing things."),
                String::from(r#"“Good enough” isn’t good enough."#),
            ]
        );

        quotes.insert(
            String::from("code"),
            vec![
                String::from("Code is a liability, not an asset. Aim to have as little of it as possible."),
                String::from("Build programs out of pure functions. This saves you from spending your brain power on tracking side effects, mutated state and actions at a distance."),
                String::from("Use a programming language with a rich type system that lets you describe the parts of your code and checks your program at compile time."),
                String::from("The expressivity of a programming language matters hugely. It’s not just a convenience to save keypresses, it directly influences the way in which you write code."),
                String::from("Choose a programming language that has a good module system, and use it. Be explicit about the public interface of a module, and ensure its interals don't leak out to client code."),
                String::from(r#"Code is a living construct that is never “done”. You need to tend it like a garden, always improving and tidying it, or it withers and dies."#),
                String::from("Have the same high standards for all the code you write, from little scripts to the inner loop of your critical system."),
                String::from("Write code that is exception safe and resource safe, always, even in contexts where you think it won’t matter. The code you wrote in a little ad-hoc script will inevitably find its way into more critical or long-running code."),
                String::from("Use the same language for the little tools and scripts in your system too. There are few good reasons to drop down into bash or Python scripts, and some considerable disadvantages."),
                String::from("In code, even the smallest details matter. This includes whitespace and layout!")
            ]
        );

        quotes.insert(
            String::from("design"),
            vec![
                String::from("Model your domain using types."),
                String::from("Model your domain first, using data types and function signatures, pick implementation technologies and physical architecture later."),
                String::from("Modelling - the act of creating models of the world - is a crucial skill, and one that’s been undervalued in recent years."),
                String::from("Implement functionality in vertical slices that span your whole system, and iterate to grow the system."),
                String::from("Resist the temptation to use your main domain types to describe interfaces or messages exchanged by your system. Use separate types for these, even if it entails some duplication, as these types will evolve differently over time."),
                String::from("Prefer immutability always. This applies to data storage as well as in-memory data structures."),
                String::from("When building programs that perform actions, model the actions as data, then write an interpreter that performs them. This makes your code much easier to test, monitor, debug, and refactor."),
                String::from("Dependency management is crucial, so do it from day one. The payoff for this mostly comes when your system is bigger, but it’s not expensive to do from the beginning and it saves massive problems later."),
                String::from("Avoid circular dependencies, always.")
            ]
        );

        quotes.insert(
            String::from("quality"),
            vec![
                String::from("I don’t care if you write the tests first, last, or in the middle, but all code must have good tests."),
                String::from("Tests should be performed at different levels of the system. Don’t get hung up on what these different levels of tests are called."),
                String::from("Absolutely all tests should be automated."),
                String::from("Test code should be written and maintained as carefully as production code."),
                String::from("Developers should write the tests."),
                String::from("Run tests on the production system too, to check it’s doing the right thing.")
            ]
        );

        quotes.insert(
            String::from("designing-systems"),
            vec![
                String::from("A better system is often a smaller, simpler system."),
                String::from("To design healthy systems, divide and conquer. Split the problem into smaller parts."),
                // String::from("Divide and conquer works recursively: divide the system into a hierarchy of simpler sub-systems and components."),
                // String::from("Corollary: When designing a system, there are more choices than a monolith vs. a thousand “microservices”."),
                String::from("The interface between parts is crucial. Aim for interfaces that are as small and simple as possible."),
                String::from("Data dependencies are insidious. Take particular care to manage the coupling introduced by such dependencies."),
                String::from("Plan to evolve data definitions over time, as they will inevitably change."),
                String::from("Asynchronous interfaces can be useful to remove temporal coupling between parts."),
                String::from("Every inter-process boundary incurs a great cost, losing type safety, an making it much harder to reason about failures. Only introduce such boundaries where absolutely necessary and where the benefits outweigh the cost."),
                String::from("Being able to tell what your system is doing is crucial, so make sure it’s observable."),
                // String::from("Telling what your system has done in the past is even more crucial, so make sure it’s auditable."),
                String::from("A modern programming language is the most expressive tool we have for describing all aspects of a system."),
                // String::from("This means: write configuration as code, unless it absolutely, definitely has to change at runtime."),
                // String::from("Also, write the specification of the system as executable code."),
                // String::from("And, use code to describe the infrastructure of your system, in the same language as the rest of the code. Write code that interprets the description of your system to provision actual physical infrastructure."),
                // String::from("At the risk of repeating myself: everything is code."),
                // String::from("Corollary: if you’re writing JSON or YAML by hand, you’re doing it wrong. These are formats for the machines, not for humans to produce and consume. (Don’t despair though: most people do this, I do too, so you’re not alone! Let's just try to aim for something better)."),
                String::from("The physical manifestation of your system (e.g. choices of storage, messaging, RPC technology, packaging and scheduling etc) should usually be an implementation detail, not the main aspect of the system that the rest is built around."),
                String::from("It should be easy to change the underlying technologies (e.g. for data storage, messaging, execution environment) used by a component in your system, this should not affect large parts of your code base."),
                String::from("You should have at least two physical manifestations of your system: a fully integrated in-memory one for testing, and the real physical deployment. They should be functionally equivalent."),
                String::from("You should be able to run a local version of your system on a developer’s computer with a single command. With the capacity of modern computers, there is absolutely no rational reason why this isn’t feasible, even for big, complex systems."),
                // String::from("There is a running theme here: separate the description of what a system does from how it does it. This is probably the single most important consideration when creating a system.")
            ]
        );

        quotes
    }
}
