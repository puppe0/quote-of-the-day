# Quote of the day

Get a random quote of the day. Quotes are provided by https://gist.github.com/stettix/5bb2d99e50fdbbd15dd9622837d14e2b

## API

### `GET /quotes/random`

Get a random quote from random category.

Response: 

```json
{
  "category": "fundamentals",
  "quote": "You should try to talk about what you’re planning to do before you do it."
}
```

### `GET /quotes/<category>`

Get a quote from a specific category.

Request:

`GET /quotes/code`

Response:

```json
{
  "category": "code",
  "quote": "In code, even the smallest details matter. This includes whitespace and layout!"
}
```
