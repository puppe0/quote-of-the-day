#[cfg(test)]
mod tests {
    use actix_web::{http::StatusCode, test, App};
    use quote_of_the_day::api::{quote_by_category, random_quote, QuoteResponse};

    #[actix_rt::test]
    async fn test_quote_by_category_is_found() {
        let mut app = test::init_service(App::new().service(quote_by_category)).await;
        let request = test::TestRequest::with_uri("/quotes/code").to_request();
        let response = test::call_service(&mut app, request).await;

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[actix_rt::test]
    async fn test_quote_by_category_is_not_found() {
        let mut app = test::init_service(App::new().service(quote_by_category)).await;
        let request = test::TestRequest::with_uri("/quotes/not_found").to_request();
        let response = test::call_service(&mut app, request).await;

        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }

    #[actix_rt::test]
    async fn test_quote_by_category_response() {
        let mut app = test::init_service(App::new().service(quote_by_category)).await;
        let request = test::TestRequest::with_uri("/quotes/code").to_request();
        let response: QuoteResponse = test::read_response_json(&mut app, request).await;

        assert_eq!(response.quote.is_empty(), false);
        assert_eq!(response.category, "code");
    }

    #[actix_rt::test]
    async fn test_random_quote_response() {
        let mut app = test::init_service(App::new().service(random_quote)).await;
        let request = test::TestRequest::with_uri("/quotes/random").to_request();
        let response: QuoteResponse = test::read_response_json(&mut app, request).await;

        assert_eq!(response.quote.is_empty(), false);
        assert_eq!(response.category.is_empty(), false);
        assert_ne!(response.category, "random");
    }
}
